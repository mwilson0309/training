package Inheritance;

public class HomeInsurance implements Detailable{
    private double amountInsured;
    private double excess;
    private double premium;
    
    public HomeInsurance(double amtI, double exc, double prem){
        this.amountInsured = amtI;
        this.excess = exc;
        this.premium = prem;
    }

    @Override
    public String getDetails() {
        return "Premium: " + premium + " Excess: " + excess;
    }
}