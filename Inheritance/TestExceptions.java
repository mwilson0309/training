package Inheritance;

public class TestExceptions {
    public static void main(String[] args) {
        try {
            Account accounts[] = new Account[3];
            accounts[0] = new SavingsAccount("Fingers", 4.0);
            accounts[1] = new SavingsAccount("Name", 2.0);
            accounts[2] = new CurrentAccount("Test", 5.0);
        } catch (DodgyNameException e) {
            
            System.out.println(e.toString());

        }
    }
}