package Inheritance;

public class CurrentAccount extends Account{
    public CurrentAccount(String name, double balance) throws DodgyNameException {
        super(name, balance);
    }

    @Override
    public void addInterest(){
        setBalance(getBalance()* 1.1);
    }

}