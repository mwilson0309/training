package Inheritance;

public class TestInterfaces {
    public static void main(String[] args) throws DodgyNameException {
        Detailable array[] = new Detailable[3];
        array[0] = new CurrentAccount("Test", 1.0);
        array[1] = new SavingsAccount("Test1", 2.0);
        array[2] = new HomeInsurance(1.0, 2.0, 3.0);

        for(int i=0;i<3;i++){
            String details = array[i].getDetails();
            System.out.println(details);
        }
    }
}