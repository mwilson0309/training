package Inheritance;

public class TestInheritance {
    public static void main(String[] args) throws DodgyNameException {
        Account accounts[] = new Account[3];
        accounts[0] = new SavingsAccount("Why", 4.0);
        accounts[1] = new SavingsAccount("Name", 2.0);
        accounts[2] = new CurrentAccount("Test", 5.0);

        for (int i=0;i<3;i++){
            String name = accounts[i].getName();
            double bal = accounts[i].getBalance();
            accounts[i].addInterest();
            double newbal = accounts[i].getBalance();
            System.out.println("Name: "+name+" | Old Bal: "+bal+" | New Bal: "+newbal);
        }


    }
}