package Inheritance;

public abstract class Account implements Detailable{
    private double balance;
    private String name;
    static double interestRate = 5;

    //Constructors:
    public Account(String n, double bal) throws DodgyNameException {
        if(n.equals("Fingers")){
            throw new DodgyNameException();
        }
        name = n;
        balance = bal;
    }
    public Account() throws DodgyNameException {
        //The instructions told me to put this here too but it's pointless b/c the name isn't given and is set after...
        if(name.equals("Fingers")){
            throw new DodgyNameException();
        }

        this.name = "Mary";
        this.balance = 50.0;
    }

    //Getters and setters:
    public double getBalance(){
        return balance;
    }
    public void setBalance(double bal){
        balance = bal;
    }
    public String getName(){
        return name;
    }
    public void setName(String n) throws DodgyNameException {
        if(n.equals("Fingers")){
            throw new DodgyNameException();
        }
        name = n;
    }
    static double getInterestRate(){
        return interestRate;
    }
    static double setInterestRate(double IR){
        interestRate = IR;
        return 0;
    }

    //Methods:
    public abstract void addInterest();

    public boolean withdraw(double amount){
        return checkBal(amount);
    }
    public boolean withdraw(){
        return checkBal(20);
    }
    public boolean checkBal(double amt){
        if ((balance - amt) < 0){
            System.out.println("Withdrawl Cancelled - Insufficient Funds");
            return false;
        }
        else{
            balance -= amt;
            System.out.println("Withdrawl Successful");
            return true;
        }
    }

    @Override
    public String getDetails() {
        return "The instructions don't say what to put here but this is for Account class.";
    }
}