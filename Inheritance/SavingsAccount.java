package Inheritance;

public class SavingsAccount extends Account{
    public SavingsAccount(String name, double balance) throws DodgyNameException {
        super(name, balance);
    }

    @Override
    public void addInterest(){
        setBalance(getBalance()* 1.4);
    }
}