package Inheritance;
import java.util.*;

public class CollectionsTest {
    public static void main(String[] args) throws DodgyNameException {
        //HashSet<Account> accounts;
        //accounts = new HashSet<Account>();
        TreeSet<Account> accounts;
        //okay so the assignment says to order it by balance but I couldn't find a lambda comparator that uses doubles so I did the lambda version with names.
        accounts = new TreeSet<Account>((a1,a2) ->a1.getName().compareTo(a2.getName()));//new AccountComparator() //taken out for lambda expression
        
        Account acct1 = new SavingsAccount("ATest1", 3.0);
        Account acct2 = new SavingsAccount("CTest2", 2.0);
        Account acct3 = new CurrentAccount("BTest3", 7.0);
        accounts.add(acct1);
        accounts.add(acct2);
        accounts.add(acct3);

        System.out.println("With iterator:");
        Iterator<Account> itr = accounts.iterator();
        while(itr.hasNext()){
            Account acct = itr.next();
            String name = acct.getName();
            double balance = acct.getBalance();
            acct.addInterest();
            double newbal = acct.getBalance();
            System.out.println("Name: "+name+" | OldBal: "+balance+" | NewBal: "+newbal);
        }

        System.out.println();
        System.out.println("With for each construct:");
        for (Account a: accounts){
            String n = a.getName();
            double bal = a.getBalance();
            a.addInterest();
            double newbal = a.getBalance();
            System.out.println("Name: "+n+" | OldBal:"+bal+" | NewBal: "+newbal);
        }

        System.out.println();
        System.out.println("With forEach method:");
        accounts.forEach(a->System.out.println("Name: "+a.getName()+" | OldBal: "+a.getBalance()) );
        accounts.forEach(a->a.addInterest());
        accounts.forEach(a->System.out.println("NewBal: "+ a.getBalance()));
    }
}