package Account;

public class TestStrings {
    public static void main(String[] args) {
        //2
        String s = "example.doc";
        s = "example.bak";
        System.out.println(s); //works

        //3 - This one was only annoying because of the wording...
        String s1 = "blah";
        String s2 = "test";
        int check = s1.compareTo(s2);
        if (check == 0){
            System.out.println("Strings are equal");
        }
        else if (check < 0){
            System.out.println("S1 is lexicographically further forward than S2.");
        }
        else {
            System.out.println("S2 is lexicographically further forward than S1.");
        }

        //4
        //Honestly I need to move on, this is just a substring function.

        //5 
        String str = "abba";
        boolean bool = true;
        int i = 0;
        int j = str.length();
        while (bool){
            if(str.charAt(i) != str.charAt(j)){ 
                bool = false;
            }
            i++;
            j--;
        }
        if (!bool){
            System.out.println("Not a palindrome.");
        }
        else {
            System.out.println("Is a palindrome.");
        }
    }

}