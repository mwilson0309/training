package Account;

public class TestAccount2 {
    public static void main(String[] args) {
        //Chapter 8:
        Account arrayOfAccounts[] = new Account[5];
        arrayOfAccounts[0] = new Account("Picard", 25);
        arrayOfAccounts[1] = new Account("Ryker", 5444);
        arrayOfAccounts[2] = new Account("Worf", 2);
        arrayOfAccounts[3] = new Account("Troy", 345);
        arrayOfAccounts[4] = new Account("Data", 34);

        Account.setInterestRate(7);

        for(int i = 0; i < 5; i++){
            String n = arrayOfAccounts[i].getName();
            double bb = arrayOfAccounts[i].getBalance(); //Before Balance
            arrayOfAccounts[i].addInterest();
            double ab = arrayOfAccounts[i].getBalance(); //After Balance

            System.out.println("Name: "+n+" | Before Bal: "+bb+ " | After Bal: "+ab);
        }
        //System.out.println("IR: "+Account.getInterestRate());

        arrayOfAccounts[0].withdraw(30);
    }
}