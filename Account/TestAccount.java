package Account;

public class TestAccount{
    public static void main(String[] args){
        //Chapter 6:
        Account acct1 = new Account();
        acct1.setName("Mary");
        acct1.setBalance(1.0);

        //double bal = acct1.getBalance();
        //String name = acct1.getName();
        //System.out.println("Name: " + name + " Balance: " + bal);

        acct1.addInterest();
        //double newBal = acct1.getBalance();
        //System.out.println("Name: "+name+" New Balance: "+ newBal);

        //Chapter 7:
        Account arrayOfAccounts[] = new Account[5];
        double[] amounts = {23,5444,2,345,34};
        String[] names = {"Picard","Ryker","Worf","Troy","Data"};

        for (int i=0;i<5;i++){
            arrayOfAccounts[i] = new Account();
            arrayOfAccounts[i].setBalance(amounts[i]);
            arrayOfAccounts[i].setName(names[i]);
            arrayOfAccounts[i].addInterest();

            System.out.println("Name: " + names[i] + " | Balance After Interest: " + arrayOfAccounts[i].getBalance());
        }
    }
}