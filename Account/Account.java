package Account;

public class Account {
    private double balance;
    private String name;
    static double interestRate = 5;

    //Constructors:
    public Account(String n, double bal){
        name = n;
        balance = bal;
    }
    public Account(){
        this.name = "Mary";
        this.balance = 50.0;
    }

    //Getters and setters:
    public double getBalance(){
        return balance;
    }
    public double setBalance(double bal){
        balance = bal;
        return 0;
    }
    public String getName(){
        return name;
    }
    public String setName(String n){
        name = n;
        return "";
    }
    static double getInterestRate(){
        return interestRate;
    }
    static double setInterestRate(double IR){
        interestRate = IR;
        return 0;
    }

    //Methods:
    public double addInterest(){
        double IR = interestRate / 100;
        balance += (balance*IR);
        return 0;
    }

    public boolean withdraw(double amount){
        return checkBal(amount);
    }
    public boolean withdraw(){
        return checkBal(20);
    }
    public boolean checkBal(double amt){
        if ((balance - amt) < 0){
            System.out.println("Withdrawl Cancelled - Insufficient Funds");
            return false;
        }
        else{
            balance -= amt;
            System.out.println("Withdrawl Successful");
            return true;
        }
    }
}