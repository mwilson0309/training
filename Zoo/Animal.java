package Zoo;

import java.util.Random;

public class Animal {
    private Gender gender;
    private Diet diet;
    private Food[] food = new Food[]{}; //this still needs to be reworked, but I'm leaving it for now, I know its not right
    private double temp;
    private int litter;

    public Animal(Gender g, Diet d, Food[] f, double t, int l){
        gender = g;
        diet = d;
        food = f;
        temp = t;
        litter = l;
    }

    public Gender getGender(){
        return gender;
    }
    public Diet getDiet(){
        return diet;
    }
    public Food[] getFood(){
        return food;
    }
    public double getTemp(){
        return temp;
    }
    public int getLitter(){
        return litter;
    }

    public int giveBirth() throws WrongGenderException{
        int result = 0;
        //throws WrongGenderException if male
        if (gender.compareTo(Gender.MALE) == 0){
            throw new WrongGenderException();
        }

        //10% chance of success of method
        if (Math.random() <= 0.1){
            //Success, babies!
            Random babies = new Random();
            result = babies.nextInt(litter);
            //returns # of new babies up to max litter based on animal type
        }

        //if unsuccessful, 0 babies are produced
        return result;
    }

    public boolean heartbeat(){
        if (Math.random() <= 0.8){
            return true;
        }
        else {
            return false;
        }
    }

    public static Gender genderCoinFlip() {
        Gender g;
        Random rand = new Random();
        int result = rand.nextInt(2);
        if (result == 0){
            g = Gender.MALE;
        }
        else{
            g = Gender.FEMALE;
        }
        return g;
    }
}