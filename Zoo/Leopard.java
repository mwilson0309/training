package Zoo;

import java.util.Random;

public class Leopard extends Animal {
    public static Food[] f = new Food[] { Food.MEAT };
    public static Diet d = Diet.CARNIVORE;
    public static double t = 101.3;
    public static int l = 4;

    public Leopard(Gender g) {
        super(g, d, f, t, l); 
    }
    public Leopard(){ //This one is for random gender
        super(genderCoinFlip(), d, f, t, l);
    }
}