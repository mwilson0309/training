package Zoo;

import java.util.Random;

public class Zebra extends Animal {
    public static Food[] f = new Food[] { Food.GRASS };
    public static Diet d = Diet.HERBIVORE;
    public static double t = 100.0;
    public static int l = 1;

    public Zebra(Gender g) {
        super(g, d, f, t, l); 
    }
    public Zebra(){ //This one is for random gender
        super(genderCoinFlip(), d, f, t, l);

    }
}