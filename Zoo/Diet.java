package Zoo;

public enum Diet {
    HERBIVORE, CARNIVORE, OMNIVORE
}