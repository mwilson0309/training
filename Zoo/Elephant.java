package Zoo;

import java.util.Random;

public class Elephant extends Animal{
    public static Food[] f = new Food[] { Food.GRASS, Food.FRUIT };
    public static Diet d = Diet.HERBIVORE;
    public static double t = 96.6;
    public static int l = 1;

    public Elephant(Gender g) {
        super(g, d, f, t, l); 
    }
    public Elephant(){ //This one is for random gender
        super(genderCoinFlip(), d, f, t, l);

    }

}