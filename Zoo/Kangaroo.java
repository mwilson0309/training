package Zoo;

import java.util.Random;

public class Kangaroo extends Animal {
    public static Food[] f = new Food[] { Food.GRASS };
    public static Diet d = Diet.HERBIVORE;
    public static double t = 97.0;
    public static int l = 2;

    public Kangaroo(Gender g) {
        super(g, d, f, t, l); 
    }
    public Kangaroo(){ //This one is for random gender
        super(genderCoinFlip(), d, f, t, l);

    }
}