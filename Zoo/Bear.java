package Zoo;

import java.util.Random;

public class Bear extends Animal {
    public static Food[] f = new Food[] { Food.MEAT, Food.FISH };
    public static Diet d = Diet.CARNIVORE;
    public static double t = 100.4;
    public static int l = 2;

    public Bear(Gender g) {
        super(g, d, f, t, l); 
    }
    public Bear(){ //This one is for random gender
        super(genderCoinFlip(), d, f, t, l);
    }

    
    
}