package Zoo;

public class WrongGenderException extends Exception{
    public WrongGenderException() {
        super("Males cannot give birth!");
    }
}