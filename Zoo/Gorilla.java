package Zoo;

import java.util.Random;

public class Gorilla extends Animal {
    public static Food[] f = new Food[] { Food.GRASS, Food.FRUIT };
    public static Diet d = Diet.HERBIVORE;
    public static double t = 95.9;
    public static int l = 1;

    public Gorilla(Gender g) {
        super(g, d, f, t, l); 
    }
    public Gorilla(){ //This one is for random gender
        super(genderCoinFlip(), d, f, t, l);

    }
}