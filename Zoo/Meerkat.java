package Zoo;

import java.util.Random;

public class Meerkat extends Animal {
    public static Food[] f = new Food[] { Food.BUGS, Food.GRASS, Food.EGGS };
    public static Diet d = Diet.OMNIVORE;
    public static double t = 100.9;
    public static int l = 8;

    public Meerkat(Gender g) {
        super(g, d, f, t, l); 
    }
    public Meerkat(){ //This one is for random gender
        super(genderCoinFlip(), d, f, t, l);

    }
}