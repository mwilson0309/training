package Zoo;

import java.util.Random;

public class Warthog extends Animal {
    public static Food[] f = new Food[] { Food.FRUIT, Food.GRASS, Food.EGGS };
    public static Diet d = Diet.OMNIVORE;
    public static double t = 80.0;
    public static int l = 8;

    public Warthog(Gender g) {
        super(g, d, f, t, l); 
    }
    public Warthog(){ //This one is for random gender
        super(genderCoinFlip(), d, f, t, l);

    }
}